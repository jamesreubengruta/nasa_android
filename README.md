# NASA_ANDROID

NASA API showcase using Android's Jetpack Compose
featuring industry standard setup.
- Multi modular architecture
- MVVM
- Autogen Navigation

For detailed info: 
https://wary-shield-e2a.notion.site/NASA-API-60b38ef2ff684d6aa0aced2ae532a396?pvs=4

For UI/UX design (still being updated):
https://www.figma.com/file/6oFVw6bjJF8wx5nSBfKffS/NASA?type=design&node-id=0%3A1&mode=design&t=C99Tq0Iw5Q4CU3oT-1


## Basic commit rules

Use the following guidelines for commit

feat: A new feature for the user. (add login functionality)

fix: A bug fix for the user. (resolve issue with user authentication)

chore: Routine tasks, maintenance, or tooling changes. (update dependencies)

docs: Changes to documentation. (update installation instructions)

style: Code style changes (e.g., formatting). (format code according to style guide)

refactor: Code changes that neither fix a bug nor add a feature. (simplify user validation logic)

test: Adding or modifying tests. (add unit tests for user service)
